import java.util.*;

final int ROWS = 34, COLUMNS = 26;
final int CELL_SIZE = 20;
final int BOARD_WIDTH = COLUMNS*CELL_SIZE, BOARD_HEIGHT = ROWS*CELL_SIZE;
float scaleFactor=1;
final float zoomRatio = 1.2;

float originX=0; 
float originY=0;
float focusX; 
float focusY;
int days=0;

Board board;
//Board pasteboard;
Cell[][] cells;

ArrayList<Integer> numbers = new ArrayList<Integer>();


void setup()
{
  //println(BOARD_WIDTH, BOARD_HEIGHT);
  size(520, 680); // WARNNING!!! you need to change the canvas size as BOARD_WIDTH and BOARD_HEIGHT checking with above line
  board = new Board(false);
  //pasteboard = new Board(false);
  cells = board.getCells();

  background(255);
  zoom(focusX, focusY, scaleFactor);
  fill(255);
  textSize(15);
  text("days: "+days, 15, height-30);
  numbers.add(0);
}

void draw()
{
  background(255);
  zoom(focusX, focusY, scaleFactor);
  
  
  
  if (numbers.get(0)==0) {    
    fill(255);
    textSize(15);
    text("days: "+days, 15, height-30);
  }

  if (numbers.get(0)==1) { //editmode
    fill(255);
    textSize(15);
    days=0;
    text("days: "+days, 15, height-30);
  }

  if (numbers.get(0)==2) { //playmode
    Cell[][] nextGeneration = board.nextGeneration();
    
    //if(check_days())  days++;
    board.setCells(nextGeneration);
    cells = board.getCells();
    fill(255);
    textSize(15);
    text("days: "+days, 15, height-30);
    delay(100);
  }
}

boolean check_days()
{
  boolean check = false;
  //all cells are dead
  for (int row = 0; row<ROWS; row++)
  {
    for (int col = 0; col<COLUMNS; col++)
    {
      if(!(cells[row][col].getState() == "Dead")) check = true;
    }
  }
  
  //no change
  
  return check;
}

void mousePressed() //for making living cells
{
  for (int row = 0; row<ROWS; row++)
  {
    for (int col = 0; col<COLUMNS; col++)
    {
      cells[row][col].isPressed(mouseX, mouseY);
    }
  }
}

void mouseDragged() //for making living cells
{
  for (int row = 0; row<ROWS; row++)
  {
    for (int col = 0; col<COLUMNS; col++)
    {
      cells[row][col].isPressed(mouseX, mouseY);
    }
  }
}

void keyPressed() //for simulating each input.
{
  if (key == 'e' || key == 'E') //preesing 'e' (or 'E') will change the editing mode equal to 5 fingers input
  {
    if (numbers.get(0)==0 || numbers.get(0)==2) {
      numbers.remove(0); 
      numbers.add(1); 
      println("1changed");
    } else if (numbers.get(0)==1) {
      numbers.remove(0); 
      numbers.add(2); 
      println("2changed");
    }
    board.setEditMode(!board.getEditMode()); //
    println("Edit mode is changed!!!");
  }

  if (key == 'c' || key == 'C') //preesing 'c' (or 'C') will erase the board
  {
    if (numbers.get(0)==1) {
      board = new Board(true);
      cells = board.getCells();
      println("The board is cleaned!!!");
      days=0;
    }
  }

  if (key == '=') // pressing '=' will zoom in, with the focus of mouse position
  {
    scaleFactor *= zoomRatio;
    setFocus(zoomRatio);
    println("The board zoomed in, scaleFactor : "+ scaleFactor);
  }

  if (key == '-') // pressing '-' will zoom out, with the focus of mouse position
  {
    scaleFactor /= zoomRatio;
    if (scaleFactor<1) scaleFactor =1;

    setFocus(1/zoomRatio);
    println("The board zoomed out, scaleFactor : " + scaleFactor);
  }

  if (key == 'a' || key == 'A')
  {
    board.additionalfeature();
    cells = board.getCells();
  }
}

abstract class Cell extends Observable
{
  protected String state; //either "Alive" or "Dead"
  protected int x, y, row, col;
  public color linecol;
  private Cell[][] cells;
  //protected int livecells;

  Cell(String state, int row, int col)
  {
    //this.livecells = 0;
    this.state = state;
    this.row = row;
    this.col = col;
    linecol = color(0);
    y = (row)*CELL_SIZE + CELL_SIZE/2;
    x = (col)*CELL_SIZE + CELL_SIZE/2;
  }

  void draw()
  {
    rectMode(CENTER);
    stroke(linecol);
    strokeWeight(1.3);
    if (state == "Alive") {
      fill(147, 230, 0);
    } else {
      fill(0);
    }
    rect(x, y, CELL_SIZE, CELL_SIZE);
  }

  int NumOfLive() //check surrounding
  {  
    int num = 0;

    if (row==0) {
      if (col==0) {
        if (cells[row][col+1].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else if (col==COLUMNS-1) {
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else {
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
      }
    } else if (row==ROWS-1) {
      if (col==0) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
      } else if (col==COLUMNS-1) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row][col-1].getState()=="Alive") num++;
      } else {
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
      }
    } else {
      if (col==0) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else if (col==COLUMNS-1) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else {
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
      }
    }
    return num;
  }

  void isPressed(float x, float y)
  {
    //this part is for fixing the gap occured by the Matrix function
    x -= focusX; 
    y -= focusY;
    x /= scaleFactor ; 
    y /= scaleFactor;

    if (x<this.x-CELL_SIZE/2 || x>this.x+CELL_SIZE/2) return;
    if (y<this.y-CELL_SIZE/2 || y>this.y+CELL_SIZE/2) return;
    setChanged();
    notifyObservers();
  }

  String getState()
  {
    return state;
  }

  int getrow()
  {
    return row;
  }

  int getcol()
  {
    return col;
  }

  void setCells(Cell[][] currentCells)
  {
    cells = currentCells;   
  }

  abstract int inspectSurround();
}

class LiveCell extends Cell
{
  LiveCell(int row, int col)
  {
    super("Alive", row, col);
  }
  int inspectSurround() {
    if ((NumOfLive() == 2) || (NumOfLive() == 3)) return 0; 
    return 1;
  }
}

class DeadCell extends Cell
{
  DeadCell(int row, int col)
  {
    super("Dead", row, col);
  }

  int inspectSurround() {
    if (NumOfLive() == 3) return 2; 
    return 0;
  }
}


class Board implements Observer
{
  private boolean isEditMode;
  private Cell[][] cells;

  Board(boolean isEditMode)
  {
    this.isEditMode = isEditMode;
    cells = new Cell[ROWS][COLUMNS];
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        DeadCell deadcell = new DeadCell(row, col);
        deadcell.addObserver(this); //register this board as observer on each cell
        cells[row][col] = deadcell;
      }
    }
  }

  void draw()
  {
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        if (isEditMode) cells[row][col].linecol=color(255);
        else {
          cells[row][col].linecol=color(0);
        }
        cells[row][col].draw();
      }
    }
    
  }

  public void update(Observable obs, Object obj)
  {
    if (obs instanceof Observable && isEditMode)
    {
      Cell thisCell = (Cell) obs;
      if (thisCell.getState() == "Dead")
      {
        int row = thisCell.getrow();
        int col = thisCell.getcol();
        LiveCell livecell = new LiveCell(row, col);
        livecell.addObserver(this); //register this board as observer on newly created cell
        cells[row][col] = livecell;
        thisCell.setCells(cells);
        println(thisCell.NumOfLive());
      } else {
        int row = thisCell.getrow();
        int col = thisCell.getcol();
        DeadCell deadcell = new DeadCell(row, col);
        deadcell.addObserver(this); //register this board as observer on newly created cell
        cells[row][col] = deadcell;
        thisCell.setCells(cells);
        println(thisCell.NumOfLive());
      }
    }
  }

  Cell[][] nextGeneration() 
  {
    Cell[][] nextGeneration = new Cell[ROWS][COLUMNS];
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        Cell thisCell = cells[row][col];
        nextGeneration[row][col] = thisCell;
        thisCell.setCells(cells);
        int state = thisCell.inspectSurround();
        if (state == 2 && !isEditMode)
        {
          LiveCell livecell = new LiveCell(row, col);
          livecell.addObserver(this); //register input board as observer on newly created cell
          nextGeneration[row][col] = livecell;
        } else if (state == 1  && !isEditMode)
        {
          DeadCell deadcell = new DeadCell(row, col);
          deadcell.addObserver(this); //register input board as observer on newly created cell
          nextGeneration[row][col] = deadcell;
        }
      }
    }
    return nextGeneration;
  }

  boolean getEditMode()
  {
    return isEditMode;
  }

  void setEditMode(boolean mode)
  {
    isEditMode = mode;
  }

  Cell[][] getCells()
  {
    return cells;
  }

  void setCells(Cell[][] inputCells)
  {
    cells = inputCells;
    if(check_days())  days++;
  }

  void additionalfeature() {
    if (isEditMode) {
      for (int row = 0; row<ROWS; row++)
      {
        for (int col = 0; col<COLUMNS; col++)
        {
          if (row%2 == 0&&col%2==1 || row%2==1&&col%2 == 0)
          {
            Cell thisCell = cells[row][col];
            if (thisCell.getState() == "Dead")
            {
              LiveCell livecell = new LiveCell(row, col);
              livecell.addObserver(this); //register this board as observer on newly created cell
              cells[row][col] = livecell;
              thisCell.setCells(cells);
              println(thisCell.NumOfLive());
            } else {
              DeadCell deadcell = new DeadCell(row, col);
              deadcell.addObserver(this); //register this board as observer on newly created cell
              cells[row][col] = deadcell;
              thisCell.setCells(cells);
              println(thisCell.NumOfLive());
            }
          }
        }
      }
    }
  }
}

void zoom(float x, float y, float s) {
  pushMatrix();
  translate(x, y);
  scale(s);
  board.draw();
  popMatrix();
}

void setFocus(float scaleRatio)
{
  focusX= scaleRatio* focusX + (1-scaleRatio) * mouseX;
  focusY= scaleRatio* focusY + (1-scaleRatio) * mouseY;

  if (focusX>0) focusX = 0;
  if (focusY>0) focusY = 0;
  if (focusX + scaleFactor*width<width) focusX = (1 -scaleFactor) * width;
  if (focusY + scaleFactor*height<height) focusY = (1 -scaleFactor) * height;
}
