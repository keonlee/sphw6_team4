package processing.test.template;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*;

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException;

import android.util.Log;
import android.view.MotionEvent;
import ketai.ui.*;
import ketai.sensors.*;



public class Template extends PApplet {


//number of cells from demo is 83 * 46. someone who can addify the cell size can use this information.
final int ROWS = 83, COLUMNS = 46;
final float CELL_SIZE = 24;
final float BOARD_WIDTH = COLUMNS*CELL_SIZE, BOARD_HEIGHT = ROWS*CELL_SIZE;
final float BOARD_SIZE = sqrt(pow(COLUMNS*CELL_SIZE,2) + pow(BOARD_HEIGHT,2));
float scaleFactor=1;
float zoomRatio;

float originX=0; 
float originY=0;
float focusX; 
float focusY;
int days=0;
//private float size;

Board board;
//Board pasteboard;
Cell[][] cells;

KetaiGesture gesture;
KetaiSensor sensor;

ArrayList<Integer> numbers = new ArrayList<Integer>();

  public void settings() { fullScreen();}

    public void setup()
{
  //println(BOARD_WIDTH, BOARD_HEIGHT);
   // WARNNING!!! you need to change the canvas size as BOARD_WIDTH and BOARD_HEIGHT checking with above line
    //fullScreen();
    board = new Board(false);
    gesture = new KetaiGesture(this);
    sensor = new KetaiSensor(this);
  sensor.start();
  cells = board.getCells();

  background(255);
  zoom(focusX, focusY, scaleFactor);
  fill(255);
  textSize(50);
  text("days: "+days, 15, height-30);
  numbers.add(0);
}

public void draw()
{
  background(255);
  zoom(focusX, focusY, scaleFactor);
  
  
  
  if (numbers.get(0)==0) {    
    fill(255);
    textSize(36);
    text("Days: "+days, 15, height-20);
  }

  if (numbers.get(0)==1) { //editmode
    fill(255);
    textSize(36);
    days=0;
    text("Days: "+days, 15, height-20);
  }

  if (numbers.get(0)==2) { //playmode
    Cell[][] nextGeneration = board.nextGeneration();
    
    //if(check_days())  days++;
    board.setCells(nextGeneration);
    cells = board.getCells();
    fill(255);
    textSize(36);
    text("Days: "+days, 15, height-20);
    delay(100);
  }
}

public boolean check_days()
{
  boolean check = false;
  //all cells are dead
  for (int row = 0; row<ROWS; row++)
  {
    for (int col = 0; col<COLUMNS; col++)
    {
      if(!(cells[row][col].getState() == "Dead")) check = true;
    }
  }
  
  //no change
  
  return check;
}


  public void onTap(float x, float y) //for making living cells
  {
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        cells[row][col].isPressed(mouseX, mouseY);
        println("TestExercise", "I am tapped");
      }
    }
  }

/*public void mousePressed() //for making living cells
{
  for (int row = 0; row<ROWS; row++)
  {
    for (int col = 0; col<COLUMNS; col++)
    {
      cells[row][col].isPressed(mouseX, mouseY);
    }
  }
}*/

public void mouseDragged() //for making living cells
{
  for (int row = 0; row<ROWS; row++)
  {
    for (int col = 0; col<COLUMNS; col++)
    {
      cells[row][col].isPressed(mouseX, mouseY);
    }
  }
}

  public void onPinch(float x, float y, float d) { //only in edit mode
    if (numbers.get(0)==1) {
      Log.d("Pinch", "x : " + x + " y : " + y + " d: " + d);
      zoomRatio = (BOARD_SIZE + d) / BOARD_SIZE;
      scaleFactor *= zoomRatio;
      if (scaleFactor < 1) scaleFactor = 1;
      setFocus(x, y, zoomRatio);
    }

  }


//  public void onPinch(float x, float y, float d) { //only in edit mode
//    //if (numbers.get(0)==1)
//    {
//      //scaleFactor *= d/2;
//      scaleFactor = constrain(d, 10, 1000);
//      setFocus(scaleFactor);
//      println("TestExercise", "I am pinched", d, scaleFactor);
//    }
//  }

  //additional feature
  public void onLongPress(float x, float y)
  {
    board.additionalfeature();
    cells = board.getCells();
    println("TestExercise", "double tap");
  }

//when device is shaked
public void onAccelerometerEvent(float x, float y, float z)
  {
    if (numbers.get(0)==1) {
      if (x >= 7 || y >= 9) {
        board = new Board(true);
        cells = board.getCells();
        println("TestExercise", "The board is cleaned!!!", x, y, z);
        days = 0;
      }
    }
  }

/*public void keyPressed() //for simulating each input.
{
  if (key == 'e' || key == 'E') //preesing 'e' (or 'E') will change the editing mode equal to 5 fingers input
  {
    if (numbers.get(0) == 0 || numbers.get(0) == 2) {
      numbers.remove(0);
      numbers.add(1);
      println("1changed");
    } else if (numbers.get(0) == 1) {
      numbers.remove(0);
      numbers.add(2);
      println("2changed");
    }
    board.setEditMode(!board.getEditMode()); //
    println("Edit mode is changed!!!");
  }


  if (key == 'c' || key == 'C') //preesing 'c' (or 'C') will erase the board
  {
    if (numbers.get(0) == 1) {
      board = new Board(true);
      cells = board.getCells();
      println("The board is cleaned!!!");
      days = 0;
    }
  }
*/

/*if (key == '=') // pressing '=' will zoom in, with the focus of mouse position
  {
    scaleFactor *= zoomRatio;
    setFocus(zoomRatio);
    println("The board zoomed in, scaleFactor : "+ scaleFactor);
  }

  if (key == '-') // pressing '-' will zoom out, with the focus of mouse position
  {
    scaleFactor /= zoomRatio;
    if (scaleFactor<1) scaleFactor =1;

    setFocus(1/zoomRatio);
    println("The board zoomed out, scaleFactor : " + scaleFactor);
  }

  if (key == 'a' || key == 'A')
  {
    board.additionalfeature();
    cells = board.getCells();
  }
}*/

  public boolean surfaceTouchEvent(MotionEvent event) {

    //call to keep mouseX, mouseY, etc updated
    super.surfaceTouchEvent(event);

    int pointerIndex = event.getActionIndex();
    int pointerId = event.getPointerId(pointerIndex);
    //int pointerCount = event.getPointerCount();

    if (pointerIndex == 4)
    {
      if (numbers.get(0)==0 || numbers.get(0)==2) {
        numbers.remove(0);
        numbers.add(1);
        println("1changed");
      } else if (numbers.get(0)==1) {
        numbers.remove(0);
        numbers.add(2);
        println("2changed");
      }
      board.setEditMode(!board.getEditMode()); //
      println("TestExercise", "Edit mode is changed!!!");
    }

    //forward event to class for processing
    return gesture.surfaceTouchEvent(event);
  }

abstract class Cell extends Observable
{
  protected String state; //either "Alive" or "Dead"
  protected float x, y;
  protected int row, col;
  public int linecol;
  private Cell[][] cells;
  //protected int livecells;

  Cell(String state, int row, int col)
  {
    //this.livecells = 0;
    this.state = state;
    this.row = row;
    this.col = col;
    linecol = color(0);
    y = (row)*CELL_SIZE + CELL_SIZE/2;
    x = (col)*CELL_SIZE + CELL_SIZE/2;
  }

  public void draw()
  {
    rectMode(CENTER);
    stroke(linecol);
    strokeWeight(1.3f);
    if (state == "Alive") {
      fill(147, 230, 0);
    } else {
      fill(0);
    }
    rect(x, y, CELL_SIZE, CELL_SIZE);
  }

  public int NumOfLive() //check surrounding
  {  
    int num = 0;

    if (row==0) {
      if (col==0) {
        if (cells[row][col+1].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else if (col==COLUMNS-1) {
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else {
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
      }
    } else if (row==ROWS-1) {
      if (col==0) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
      } else if (col==COLUMNS-1) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row][col-1].getState()=="Alive") num++;
      } else {
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
      }
    } else {
      if (col==0) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else if (col==COLUMNS-1) {
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
      } else {
        if (cells[row-1][col-1].getState()=="Alive") num++;
        if (cells[row-1][col].getState()=="Alive") num++;
        if (cells[row-1][col+1].getState()=="Alive") num++;
        if (cells[row][col-1].getState()=="Alive") num++;
        if (cells[row][col+1].getState()=="Alive") num++;
        if (cells[row+1][col-1].getState()=="Alive") num++;
        if (cells[row+1][col].getState()=="Alive") num++;
        if (cells[row+1][col+1].getState()=="Alive") num++;
      }
    }
    return num;
  }

  public void isPressed(float x, float y)
  {
    //this part is for fixing the gap occured by the Matrix function
    x -= focusX; 
    y -= focusY;
    x /= scaleFactor ; 
    y /= scaleFactor;

    if (x<this.x-CELL_SIZE/2 || x>this.x+CELL_SIZE/2) return;
    if (y<this.y-CELL_SIZE/2 || y>this.y+CELL_SIZE/2) return;
    setChanged();
    notifyObservers();
  }

  public String getState()
  {
    return state;
  }

  public int getrow()
  {
    return row;
  }

  public int getcol()
  {
    return col;
  }

  public void setCells(Cell[][] currentCells)
  {
    cells = currentCells;   
  }

  public abstract int inspectSurround();
}

class LiveCell extends Cell
{
  LiveCell(int row, int col)
  {
    super("Alive", row, col);
  }
  public int inspectSurround() {
    if ((NumOfLive() == 2) || (NumOfLive() == 3)) return 0; 
    return 1;
  }
}

class DeadCell extends Cell
{
  DeadCell(int row, int col)
  {
    super("Dead", row, col);
  }

  public int inspectSurround() {
    if (NumOfLive() == 3) return 2; 
    return 0;
  }
}


class Board implements Observer
{
  private boolean isEditMode;
  private Cell[][] cells;

  Board(boolean isEditMode)
  {
    this.isEditMode = isEditMode;
    cells = new Cell[ROWS][COLUMNS];
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        DeadCell deadcell = new DeadCell(row, col);
        deadcell.addObserver(this); //register this board as observer on each cell
        cells[row][col] = deadcell;
      }
    }
  }

  public void draw()
  {
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        if (isEditMode) cells[row][col].linecol=color(255);
        else {
          cells[row][col].linecol=color(0);
        }
        cells[row][col].draw();
      }
    }
    
  }

  public void update(Observable obs, Object obj)
  {
    if (obs instanceof Observable && isEditMode)
    {
      Cell thisCell = (Cell) obs;
      if (thisCell.getState() == "Dead")
      {
        int row = thisCell.getrow();
        int col = thisCell.getcol();
        LiveCell livecell = new LiveCell(row, col);
        livecell.addObserver(this); //register this board as observer on newly created cell
        cells[row][col] = livecell;
        thisCell.setCells(cells);
        println(thisCell.NumOfLive());
      } else {
        int row = thisCell.getrow();
        int col = thisCell.getcol();
        DeadCell deadcell = new DeadCell(row, col);
        deadcell.addObserver(this); //register this board as observer on newly created cell
        cells[row][col] = deadcell;
        thisCell.setCells(cells);
        println(thisCell.NumOfLive());
      }
    }
  }

  public Cell[][] nextGeneration() 
  {
    Cell[][] nextGeneration = new Cell[ROWS][COLUMNS];
    for (int row = 0; row<ROWS; row++)
    {
      for (int col = 0; col<COLUMNS; col++)
      {
        Cell thisCell = cells[row][col];
        nextGeneration[row][col] = thisCell;
        thisCell.setCells(cells);
        int state = thisCell.inspectSurround();
        if (state == 2 && !isEditMode)
        {
          LiveCell livecell = new LiveCell(row, col);
          livecell.addObserver(this); //register input board as observer on newly created cell
          nextGeneration[row][col] = livecell;
        } else if (state == 1  && !isEditMode)
        {
          DeadCell deadcell = new DeadCell(row, col);
          deadcell.addObserver(this); //register input board as observer on newly created cell
          nextGeneration[row][col] = deadcell;
        }
      }
    }
    return nextGeneration;
  }

  public boolean getEditMode()
  {
    return isEditMode;
  }

  public void setEditMode(boolean mode)
  {
    isEditMode = mode;
  }

  public Cell[][] getCells()
  {
    return cells;
  }

  public void setCells(Cell[][] inputCells)
  {
    cells = inputCells;
    if(check_days())  days++;
  }

  public void additionalfeature() {
    if (isEditMode) {
      for (int row = 0; row<ROWS; row++)
      {
        for (int col = 0; col<COLUMNS; col++)
        {
          if (row%2 == 0&&col%2==1 || row%2==1&&col%2 == 0)
          {
            Cell thisCell = cells[row][col];
            if (thisCell.getState() == "Dead")
            {
              LiveCell livecell = new LiveCell(row, col);
              livecell.addObserver(this); //register this board as observer on newly created cell
              cells[row][col] = livecell;
              thisCell.setCells(cells);
              println(thisCell.NumOfLive());
            } else {
              DeadCell deadcell = new DeadCell(row, col);
              deadcell.addObserver(this); //register this board as observer on newly created cell
              cells[row][col] = deadcell;
              thisCell.setCells(cells);
              println(thisCell.NumOfLive());
            }
          }
        }
      }
    }
  }
}

public void zoom(float x, float y, float s) {
  pushMatrix();
  translate(x, y);
  scale(s);
  board.draw();
  popMatrix();
}

  public void setFocus(float x, float y, float d)
  {
    focusX= d* focusX + (1-d) * x;
    focusY= d* focusY + (1-d) * y;

    if (focusX>0) focusX = 0;
    if (focusY>0) focusY = 0;
    if (focusX + scaleFactor*width<width) focusX = (1 -scaleFactor) * width;
    if (focusY + scaleFactor*height<height) focusY = (1 -scaleFactor) * height;
  }

//public void setFocus(float scaleRatio)
//{
//  focusX= scaleRatio* focusX + (1-scaleRatio) * mouseX;
//  focusY= scaleRatio* focusY + (1-scaleRatio) * mouseY;
//
//  if (focusX>0) focusX = 0;
//  if (focusY>0) focusY = 0;
//  if (focusX + scaleFactor*width<width) focusX = (1 -scaleFactor) * width;
//  if (focusY + scaleFactor*height<height) focusY = (1 -scaleFactor) * height;
//}
  //public void settings() {  size(520, 680); }

}